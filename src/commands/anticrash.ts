import { Logger } from "../lib/logger.js";

const logger = new Logger("AntiCrash")

export async function staticBlock() {
    if (!global.bot.isProduction) logger.warn("Skipped initialization to help assist with debugging.")
    else {
        process.on('uncaughtException', err => {
            logger.warn(`An uncaught exception was caught! Exception: ${err.stack || err}`)
        })
        process.on('unhandledRejection', err => {
            logger.warn(`An unhandled rejection was caught! Rejection: ${err ? (err as Error).stack ?? err : "<null>"}`)
        })
        logger.info("Started anticrash.")
    }
}