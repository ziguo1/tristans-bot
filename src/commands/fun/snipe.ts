import { EmbedBuilder, RGBTuple } from "@discordjs/builders";
import { ChatInputCommandInteraction, SlashCommandBuilder } from "discord.js";
import { CommandPreprocessor } from "../../lib/preprocessor/commandPreprocessor.js";
import { CooldownDate } from "../../lib/preprocessor/cooldownDate.js";
import { DBAttachment, DeletedMessage, EditedMessage, getSnipelist } from "./snipe_dbAdapter.js";

const DELETED: RGBTuple = [222, 73, 73]
const EDITED: RGBTuple = [250, 218, 60]

export const slashCommand = new SlashCommandBuilder()
    .setName("snipe")
    .setDescription("Snipe a recently deleted/edited message.")

export const preprocessorOptions = new CommandPreprocessor({
    cooldown: new CooldownDate({ seconds: 10 }),
    serverOnly: true
})

export async function execute(i: ChatInputCommandInteraction) {
    const fetchData = await getSnipelist(i.channelId)
    if (fetchData.sniped_message == null) {
        await i.reply({
            content: "There aren't any sniped messages here!",
            ephemeral: true
        })
        return false
    } else {
        const embed = new EmbedBuilder()
            .setTitle(fetchData.sniped_message.type == 'DELETED' ? 'Message Deleted' : 'Message Edited')
            .setAuthor({ name: fetchData.sniped_message.author.username + ` (${fetchData.sniped_message.author.id})`, iconURL: fetchData.sniped_message.author.avatar })
            .setColor(fetchData.sniped_message.type == 'DELETED' ? DELETED : EDITED)
            .setTimestamp()
        if (fetchData.sniped_message.type == 'DELETED') {
            embed.setDescription(clampString((fetchData.sniped_message as DeletedMessage).content, 4096) ?? "<no content>")
            const appendData = formatAttachments((fetchData.sniped_message as DeletedMessage).attachments)
            if (appendData.length > 0) embed.addFields({ name: "Attachments", value: appendData, inline: false })
        } else {
            embed.addFields({
                name: "Old Content",
                value: clampString((fetchData.sniped_message as EditedMessage).content.old, 1024) ?? "<nothing>",
                inline: true
            }, {
                name: "New Content",
                value: clampString((fetchData.sniped_message as EditedMessage).content.new, 1024) ?? "<nothing>",
                inline: true
            }, {
                name: "\u200b",
                value: "\u200b",
                inline: true
            }, {
                name: "Old Attachments",
                value: formatAttachments((fetchData.sniped_message as EditedMessage).attachments.old) || "<nothing>",
                inline: true
            }, {
                name: "New Attachments",
                value: formatAttachments((fetchData.sniped_message as EditedMessage).attachments.new) || "<nothing>",
                inline: true
            })
        }
        await i.reply({ embeds: [embed] })
        return true
    }
}

function formatAttachments(d: DBAttachment[]): string {
    let appendData = ""
    d.forEach(({ name, url }) => appendData += `[${name}](${url})\n`)
    return appendData
}

function clampString(str: string, len: number): string {
    if (str == null) return null
    return str.length > len ? str.substring(0, 1021) + "..." : str
}