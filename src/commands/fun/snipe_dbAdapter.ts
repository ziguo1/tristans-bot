import { Collection, Db, WithId } from "mongodb"
import { init, mongoDb } from "../database.js"

export type DBAttachment = {
    name: string,
    url: string
}

export type BaseSnipedMessage = {
    type: 'DELETED' | 'EDITED',
    author: {
        id: string,
        username: string,
        avatar: string
    }
}

export type DeletedMessage = BaseSnipedMessage & {
    content?: string,
    attachments: DBAttachment[],
    type: 'DELETED'
}

export type EditedMessage = BaseSnipedMessage & {
    content: { old: string, new: string },
    attachments: { old: DBAttachment[], new: DBAttachment[] },
    type: 'EDITED'
}

export type SnipeData = {
    channel_id: string,
    sniped_message: BaseSnipedMessage
}

let db: Collection

export async function getSnipelist(channelId: string): Promise<SnipeData> {
    if (!await init()) db = mongoDb.db('snipedMessages').collection('snipedMessages')
    return await db.findOne({ channel_id: channelId } as Partial<SnipeData>) as WithId<Document> & SnipeData ?? {
        channel_id: channelId,
        sniped_message: null
    }
}

export async function setSnipedMessage(channelId: string, message: BaseSnipedMessage) {
    if (!await init()) db = mongoDb.db('snipedMessages').collection('snipedMessages')
    await db.updateOne({
        channel_id: channelId
    } as Partial<SnipeData>, {
        $set: {
            channel_id: channelId,
            sniped_message: message
        }
    }, { upsert: true })
}

export async function clearSnipedMessages(channelId: string) {
    if (!await init()) db = mongoDb.db('snipedMessages').collection('snipedMessages')
    await db.deleteOne({ channel_id: channelId })
}

export async function staticBlock() {
    global.bot.djsClient.on('messageDelete', async msg => {
        if (!msg.guild) return
        await setSnipedMessage(msg.channelId, {
            author: {
                id: msg.author.id,
                username: msg.author.tag,
                avatar: msg.author.avatarURL()
            },
            content: msg.content,
            attachments: msg.attachments.map(a => { return { name: a.name, url: a.proxyURL } }),
            type: 'DELETED'
        } as DeletedMessage)
    })
    global.bot.djsClient.on('messageUpdate', async (oldMessage, newMessage) => {
        if (!oldMessage.guild) return
        await setSnipedMessage(oldMessage.channelId, {
            author: {
                id: oldMessage.author.id,
                username: oldMessage.author.tag,
                avatar: oldMessage.author.avatarURL()
            },
            content: { old: oldMessage.content, new: newMessage.content },
            attachments: { old: oldMessage.attachments.map(a => { return { name: a.name, url: a.proxyURL } }), new: newMessage.attachments.map(a => { return { name: a.name, url: a.proxyURL } }) },
            type: 'EDITED'
        } as EditedMessage)
    })
}