import { EmbedBuilder } from "@discordjs/builders";
import { ChatInputCommandInteraction, SlashCommandBuilder, Status } from "discord.js";
import { mongoDb } from "./database.js";

export const slashCommand = new SlashCommandBuilder()
    .setName("stats")
    .setDescription("Debugging statistics.")

export async function execute(i: ChatInputCommandInteraction) {
    const startEditTimer = Date.now()
    await i.deferReply()
    const endEditTimer = Date.now(), dbPing = await mongoDb.db("test").command({ ping: 1 })
    const endDbPingTimer = Date.now()
    await i.editReply({
        embeds: [
            new EmbedBuilder()
                .setTitle("Diagnostics Data")
                .setColor([255, 255, 255])
                .setDescription((endEditTimer - startEditTimer > 300 || endDbPingTimer - endEditTimer < 250) 
                    ? ":warning: High network TTL detected. Expect degraded performance."
                    : ":white_check_mark: Network TTL levels appear to be in nominal levels.")
                .addFields({
                    name: "Discord Network RTT/Ping",
                    value: `\`${endEditTimer - startEditTimer} ms\``,
                    inline: true
                }, {
                    name: "Gateway Status",
                    value: `${Object.keys(Status)[Object.values(Status).indexOf(i.client.ws.status)]}`,
                    inline: true
                }, {
                    name: "Database Network RTT/Ping",
                    value: `\`${endDbPingTimer - endEditTimer} ms\``,
                    inline: true
                })
                .setFooter({ text: `Dis. RTT: ${endEditTimer - startEditTimer < 300 ? "OK" : "HIGH_RTT"} | Db. RTT: ${endDbPingTimer - endEditTimer < 250 ? "OK" : "HIGH_RTT"}` })
        ]
    })
}