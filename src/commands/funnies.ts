import { ActivityType, GuildChannel, Message, PermissionFlagsBits } from "discord.js";

export async function staticBlock() {
    global.bot.djsClient.user.setActivity("you sleep", {
        type: ActivityType.Watching
    })
    global.bot.djsClient.on('messageCreate', msg => {
        if (msg.guild != null && (msg.channel as GuildChannel).permissionsFor(global.bot.djsClient.user).has(PermissionFlagsBits.SendMessages)) {
            let msgPromise: Promise<Message>
            if (msg.mentions.has(msg.client.user)) msgPromise = msg.reply(`Hi! I use slash commands. Please type \`/\` for a list of available commands.`)
            if (msgPromise) {
                msgPromise
                    .then(msg => setTimeout(msg.delete, 5000))
                    .catch(() => {})
            }
        }
    })
}