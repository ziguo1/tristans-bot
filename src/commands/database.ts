import { MongoClient } from "mongodb";

export let mongoDb: MongoClient

export async function init(): Promise<boolean> {
    if (mongoDb) return false
    mongoDb = await new MongoClient(process.env.MONGO_URI).connect()
    return true
}

export async function staticBlock() {
    await init()
}