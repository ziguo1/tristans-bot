import { ChatInputCommandInteraction, Client, SlashCommandBuilder } from "discord.js"

declare global {
    var bot: {
        commandMap: Map<SlashCommandBuilder, (i: ChatInputCommandInteraction) => any>,
        botToken: string,
        isProduction: boolean,
        clientId: string,
        djsClient: Client
    }
}

export {}