# Tristan's Discord Bot
Tristan's Discord bot.
## Setup
Setup instructions shown below assume basic knowledge regarding the command line and the Node.js and NPM toolchain.
### Prerequisites
- [Node.js](https://nodejs.org/en/) 18 or up (latest)
- TypeScript toolchain (`npm i typescript --save-dev --global`)
- An actual computer
- A Discord bot token with all privileged intents **enabled**
### Installation
1. Clone the project using Git.
2. Install dependencies via NPM.
3. Compile the bot.
4. Create and configure `.env` in `build`. Below is an example config to serve as a reference.
```
TOKEN=token here
MONGO_URI=mongodb uri here
```
5. Run `index.js`.
## Contributions and Issues
DM me on Discord. I will not accept PRs or issues on GitLab.
